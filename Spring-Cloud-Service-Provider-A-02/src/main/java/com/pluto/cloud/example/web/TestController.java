/*
 * Copyright 2016 AsherLi0103
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.pluto.cloud.example.web;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.pluto.cloud.example.service.TestService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.client.ServiceInstance;
import org.springframework.cloud.client.discovery.DiscoveryClient;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpSession;
import java.util.List;
import java.util.Map;

/**
 * TODO
 *
 * @author AsherLi
 * @version V1.0.00
 */
@RestController
public class TestController {

    private static final Logger logger = LoggerFactory.getLogger(TestController.class);

    @Autowired
    private DiscoveryClient client;

    @Autowired
    private TestService testService;

    @RequestMapping("/getAllUsers")
    public  List<Map<String, Object>>  getAllUsers(HttpSession session) {
        List<Map<String, Object>> result = testService.getAllUsers();
        ObjectMapper objectMapper = new ObjectMapper();

        String jsonStr = null;
        try {
            jsonStr = objectMapper.writeValueAsString(result);
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }
        ServiceInstance instance = client.getLocalServiceInstance();
        LoggerFactory.getLogger(TestController.class).info(session.getId());
        logger.info("/getAllUsers, host:" + instance.getHost() + ", service_id:" + instance.getServiceId() + ", result:" + jsonStr);
        return result;
    }
}
